FROM gradle:jdk17-alpine as BUILDER

WORKDIR /build

COPY . .

RUN gradle build --no-daemon

FROM openjdk:17-alpine as RUNTIME

EXPOSE 8080

WORKDIR /app

COPY --from=BUILDER /build/build/libs/trofki-bot-0.0.1-SNAPSHOT.jar /app/app.jar

ENTRYPOINT ["java", "-jar", "/app/app.jar"]
