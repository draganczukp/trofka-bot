package me.draganczuk.bot;


import lombok.RequiredArgsConstructor;
import me.draganczuk.bot.services.DevService;
import me.draganczuk.bot.services.TrophyService;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.Commands;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
import net.dv8tion.jda.api.interactions.commands.build.SubcommandData;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.stream.IntStream;

@Component
@RequiredArgsConstructor
public class BotMain implements CommandLineRunner {
    public static JDA jda;

    @Value("${token}")
    private String token;

    private final TrophyService trophyService;

    private final DevService devService;

    @Override
    public void run(String... args) throws Exception {
        devService.populateDB();
        jda = JDABuilder.createLight(token)
                .addEventListeners(new BotListener(trophyService))
                .setStatus(OnlineStatus.ONLINE)
                .setActivity(Activity.playing("/trofki"))
                .build();
        jda.awaitReady();

        var cmds = jda.updateCommands();

        cmds.addCommands(
                Commands.slash("ping", "Ping!"),
                Commands.slash("trofka", "Dodawaj, usuwaj i przyznawaj trofki")
                        .addSubcommands(
                                new SubcommandData("moje", "Pokazuje twoje trofki"),
                                new SubcommandData("lista", "Lista trofek i ID"),
                                new SubcommandData("daj", "Przyznaj komuś trofkę")
                                        .addOptions(
                                                new OptionData(OptionType.STRING, "id", "ID trofki").setRequired(true),
                                                new OptionData(OptionType.USER, "gracz", "Komu dać trofkę").setRequired(true)
                                        )
                                        .addOptions(
                                                IntStream.range(2, 10)
                                                        .mapToObj(i ->
                                                                new OptionData(OptionType.USER, "gracz" + i, "Kolejny użytkownik")
                                                                        .setRequired(false)).
                                                        toList()),
                                new SubcommandData("nowa", "Tworzy nową trofkę")
                                        .addOptions(
                                                new OptionData(OptionType.STRING, "id", "Wymyślone ID trofki, coś co będzie łatwe w zapamiętaniu").setRequired(true),
                                                new OptionData(OptionType.STRING, "nazwa", "Wyświetlana nazwa trofki").setRequired(true),
                                                new OptionData(OptionType.STRING, "opis", "Pełny opis trofki").setRequired(true)
                                        ),
                                new SubcommandData("zabierz", "Odbiera komuś trofkę")
                                        .addOptions(
                                                new OptionData(OptionType.USER, "gracz", "Komu zabrać trofkę").setRequired(true),
                                                new OptionData(OptionType.STRING, "id", "ID trofki").setRequired(true)
                                        )
                        )
//                ,Commands.slash("team", "Zarządzaj drużynami")
//                        .addSubcommands(
//                                new SubcommandData("lista", "Lista drużyn i ID"),
//                                new SubcommandData("nowa", "Dodaj drużynę")
//                                        .addOption(OptionType.STRING, "id", "Wymyślone ID drużyny, coś łatwego do zapamiętania")
//                                        .addOption(OptionType.STRING, "naza", "Nazwa drużyny"),
//                                new SubcommandData("dodaj", "Dodaj kogoś do drużyny")
//                                        .addOption(OptionType.STRING, "id", "ID drużyny")
//                                        .addOption(OptionType.USER, "gracz", "Kogo dodać do drużyny")
//                        )
                        ).queue();
    }
}
