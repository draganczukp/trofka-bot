package me.draganczuk.bot.repos;

import me.draganczuk.bot.entities.UserTrophy;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserTrophyRepository extends JpaRepository<UserTrophy, Long> {
    List<UserTrophy> findDistinctByUserId(String userId);

    void deleteByUserIdAndId(String userId, Long id);

    boolean existsByUserIdAndTrophy_Id(String userId, String id);



}