package me.draganczuk.bot.repos;

import me.draganczuk.bot.entities.TrophyEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TrophyRepository extends JpaRepository<TrophyEntity, String> {
}