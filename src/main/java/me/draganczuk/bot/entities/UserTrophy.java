package me.draganczuk.bot.entities;

import lombok.*;
import net.dv8tion.jda.api.entities.MessageEmbed;

import javax.persistence.*;

@Entity
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserTrophy {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    private String userId;
    @ManyToOne
    @JoinColumn(name = "trophy_id")
    private TrophyEntity trophy;

    public MessageEmbed.Field toEmbedRow() {
        return new MessageEmbed.Field(trophy.getName(), trophy.getDescription(), false);
    }
}
