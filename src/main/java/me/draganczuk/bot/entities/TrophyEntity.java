package me.draganczuk.bot.entities;

import lombok.*;
import net.dv8tion.jda.api.entities.MessageEmbed;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Objects;

@Entity
@NoArgsConstructor
@Getter
@Setter
@ToString
@Builder
@AllArgsConstructor
public class TrophyEntity {
    @Id
    @Column(name = "id", nullable = false)
    private String id;
    private String name;
    private String description;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        TrophyEntity that = (TrophyEntity) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    public MessageEmbed.Field toEmbedRow() {
        return new MessageEmbed.Field(getId(), getName(), false);
    }
}
