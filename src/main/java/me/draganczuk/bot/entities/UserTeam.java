package me.draganczuk.bot.entities;

import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class UserTeam {
    @Id
    @Column(name = "id", nullable = false)
    private String id;
    private String userId;

}
