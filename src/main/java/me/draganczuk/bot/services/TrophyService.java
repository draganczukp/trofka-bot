package me.draganczuk.bot.services;

import lombok.RequiredArgsConstructor;
import me.draganczuk.bot.entities.TrophyEntity;
import me.draganczuk.bot.entities.UserTrophy;
import me.draganczuk.bot.repos.TrophyRepository;
import me.draganczuk.bot.repos.UserTrophyRepository;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.interactions.commands.OptionMapping;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
@RequiredArgsConstructor
public class TrophyService {

    private final TrophyRepository trophyRepository;
    private final UserTrophyRepository userTrophyRepository;

    public void handleTrophyCommand(SlashCommandInteractionEvent event) {
        var subcomand = Objects.requireNonNullElse(event.getSubcommandName(), "");

        switch (subcomand) {
            case "moje" -> printOwnTrophies(event);
            case "lista" -> printAllTrophies(event);
            case "nowa" -> addNewTrophy(event);
            case "daj" -> giveTrophy(event);
            case "zabierz" -> revokeTrophy(event);
            default -> event.getHook().sendMessage("Komenda %s niewspierana".formatted(subcomand)).queue();
        }
    }

    private void revokeTrophy(SlashCommandInteractionEvent event) {
        if (!event.getMember().hasPermission(Permission.ADMINISTRATOR)) {
            event.getHook().setEphemeral(true).sendMessage("Tylko admin może to zrobić").queue();
            return;
        }
        var userID = event.getUser().getId();
        var trophyId = Long.parseLong(event.getOption("id").getAsString());

        userTrophyRepository.deleteByUserIdAndId(userID, trophyId);

        event.getHook()
                .setEphemeral(true)
                .sendMessage("%s stracił trofkę %s".formatted(event.getOption("gracz").getAsUser().getName(), trophyId))
                .queue();
    }

    private void giveTrophy(SlashCommandInteractionEvent event) {
        if (!event.getMember().hasPermission(Permission.ADMINISTRATOR)) {
            event.getHook().setEphemeral(true).sendMessage("Tylko admin może to zrobić").queue();
            return;
        }
        var userID = event.getOption("gracz").getAsUser().getId();
        var trophyId = event.getOption("id").getAsString();
        var trophy = trophyRepository.findById(trophyId).orElseThrow();
        var userTrophy = UserTrophy.builder().userId(userID).trophy(trophy).build();

        if(!userTrophyRepository.existsByUserIdAndTrophy_Id(userID, trophyId)) {
            userTrophyRepository.save(userTrophy);
        }

        for (int i = 2; i < 10; i++) {
            OptionMapping option = event.getOption("gracz" + i);
            if(option != null) {
                if(!userTrophyRepository.existsByUserIdAndTrophy_Id(option.getAsUser().getId(), trophyId)) {
                    userTrophyRepository.save(
                            UserTrophy.builder()
                                    .userId(option.getAsUser().getId())
                                    .trophy(trophy)
                                    .build()
                    );
                }
            }
        }

        event.getHook()
                .setEphemeral(true)
                .sendMessage("%s dostał trofkę %s".formatted(event.getOption("gracz").getAsUser().getName(), trophyId))
                .queue();
    }

    private void addNewTrophy(SlashCommandInteractionEvent event) {
        if (!event.getMember().hasPermission(Permission.ADMINISTRATOR)) {
            event.getHook().setEphemeral(true).sendMessage("Tylko admin może to zrobić").queue();
            return;
        }
        var id = event.getOption("id").getAsString();
        var name = event.getOption("nazwa").getAsString();
        var description = event.getOption("opis").getAsString();

        var trophy = TrophyEntity.builder().id(id).name(name).description(description).build();

        trophyRepository.save(trophy);

        event.getHook()
                .setEphemeral(true)
                .sendMessage("Trofka dodana")
                .queue();
    }

    private void printAllTrophies(SlashCommandInteractionEvent event) {
        if (!event.getMember().hasPermission(Permission.ADMINISTRATOR)) {
            event.getHook().setEphemeral(true).sendMessage("Tylko admin może to zrobić").queue();
            return;
        }
        var trophies = trophyRepository.findAll();
        var builder = new EmbedBuilder();

        builder.setTitle("Lista trofek");

        trophies.stream()
                .map(TrophyEntity::toEmbedRow)
                .forEach(builder::addField);

        var embed = builder.build();
        event.getHook().sendMessageEmbeds(embed).queue();
    }

    private void printOwnTrophies(SlashCommandInteractionEvent event) {
        var trophies = userTrophyRepository.findDistinctByUserId(event.getUser().getId());
        var builder = new EmbedBuilder();

        builder.setTitle("Trofki dla gracza %s".formatted(event.getUser().getName()));

        trophies.stream()
                .map(UserTrophy::toEmbedRow)
                .forEach(builder::addField);

        var embed = builder.build();
        event.getHook().sendMessageEmbeds(embed).queue();
    }
}
