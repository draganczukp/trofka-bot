package me.draganczuk.bot.services;

import lombok.RequiredArgsConstructor;
import me.draganczuk.bot.entities.TrophyEntity;
import me.draganczuk.bot.repos.TrophyRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class DevService {

	@Value("${dev}")
	boolean dev;

	private final TrophyRepository trophyRepository;

	public void populateDB() {
		if(dev && trophyRepository.count() == 0) {
			trophyRepository.saveAll(List.of(
				TrophyEntity.builder()
						.id("test_trophy")
						.name("Testowa trofka")
						.description("Trofka powstała tylko do testów")
						.build()
			));
		}
	}
}
