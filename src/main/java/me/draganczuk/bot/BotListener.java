package me.draganczuk.bot;

import com.sun.istack.NotNull;
import lombok.RequiredArgsConstructor;
import me.draganczuk.bot.services.TrophyService;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.events.interaction.component.ButtonInteractionEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.util.concurrent.TimeUnit;

@RequiredArgsConstructor
public class BotListener extends ListenerAdapter {

    private final TrophyService trophyService;

    @Override
    public void onSlashCommandInteraction(@NotNull SlashCommandInteractionEvent event) {
        String eventName = event.getInteraction().getName();
        event.deferReply().queue();
        switch (eventName) {
            case "ping" -> event.getHook().sendMessage("pong!").queue(message -> message.delete().queueAfter(30, TimeUnit.SECONDS));
            case "trofka" -> trophyService.handleTrophyCommand(event);
            case "team" -> {}
            default -> event.getHook().setEphemeral(true).sendMessage(String.format("Unknown command: %s", eventName)).queue();
        }
        super.onSlashCommandInteraction(event);
    }

}
