package me.draganczuk.bot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class TrofkaBotApplication {
    public static void main(String[] args) {
        SpringApplication.run(TrofkaBotApplication.class, args);
    }
}
